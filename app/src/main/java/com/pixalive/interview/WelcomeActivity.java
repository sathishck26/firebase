package com.pixalive.interview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pixalive.interview.utils.Constants;

/*
 * This is the launcher class.
 * After Delayed time moved to CreateAccountActivity.
 * */
public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity();
            }
        }, Constants.DELAY_TIME);
    }

    /*
     * ActivityStarted from WelcomeActivity class to CreateAccountActivity.class
     * */
    private void startActivity() {
        startActivity(new Intent(this, CreateAccountActivity.class));
        finish(); // Destroy the welcome screen.
    }
}
