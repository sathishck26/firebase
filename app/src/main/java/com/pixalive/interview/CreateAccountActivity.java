package com.pixalive.interview;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pixalive.interview.model.NotificationMsg;
import com.pixalive.interview.model.Users;
import com.pixalive.interview.utils.Utils;

public class CreateAccountActivity  extends AppCompatActivity {
    private static final String TAG = CreateAccountActivity.class.getSimpleName();

    private EditText mEtName;
    private EditText mEtEmail;
    private EditText mEtPhone;
    private EditText mEtDescription;
    private TextView mTvList;

    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);
        bindUI();
        //get reference to the "users" node
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("users");
        mDatabaseReference.addValueEventListener(eventListener);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w(TAG, "getInstanceId failed", task.getException());
                    return;
                }
                // Get new Instance ID token
                String token = task.getResult().getToken();
                Log.d(TAG, "onCreate "+token);
                subscribe();
            }
        });

    }

    private void bindUI(){
        mEtName = findViewById(R.id.etName);
        mEtEmail = findViewById(R.id.etEmail);
        mEtPhone = findViewById(R.id.etPhone);
        mEtDescription = findViewById(R.id.etDescription);
        mTvList = findViewById(R.id.tvList);
    }

    public void actionButtonClick(View view){
        boolean isValidData = checkValidation();
        Log.d(TAG,"isValidData : "+isValidData);
        if(isValidData){
            Users users = new Users(mEtName.getText().toString(),mEtEmail.getText().toString(),mEtPhone.getText().toString(),mEtDescription.getText().toString());
            String id = ""+System.currentTimeMillis();
            mDatabaseReference.child(id).setValue(users);
            //addUserChangeListener();
            Utils.getInstance().showAlertDialog(this, "You are successfully created account.");
        }
    }

    private boolean checkValidation(){
        mEtName.setError(null);
        mEtEmail.setError(null);
        mEtPhone.setError(null);
        mEtDescription.setError(null);
        String email = mEtEmail.getText().toString();
        String description = mEtDescription.getText().toString();
        if(TextUtils.isEmpty(mEtName.getText().toString())) {
            mEtName.setError("Enter valid Name");
            return false;
        }
        else if(TextUtils.isEmpty(email) && !Utils.getInstance().isValidEmail(email)) {
            mEtEmail.setError("Enter valid Email");
            return false;
        }
        else if(TextUtils.isEmpty(mEtPhone.getText().toString())) {
            mEtPhone.setError("Enter valid Phone number");
            return false;
        }
        else if(TextUtils.isEmpty(description) || description.length() < 150){
            mEtDescription.setError("Enter valid Description. Min 150 character");
            return false;
        }
        return true;
    }

    private void subscribe() {
        FirebaseMessaging.getInstance().subscribeToTopic("pixalivetopic")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "subscribe Failed");
                        } else {
                            Log.d(TAG, "subscribe success");
                        }
                    }
                });
    }

    ValueEventListener eventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            StringBuilder msg = new StringBuilder();
            for(DataSnapshot ds : dataSnapshot.getChildren()) {
                msg.append(ds.child("name").getValue(String.class));
                msg.append("\n");
                Log.d(TAG, "eventListener onDataChange name: "+msg);
            }
            mTvList.setText(msg.toString());
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };

}
