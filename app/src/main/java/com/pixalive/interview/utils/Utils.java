package com.pixalive.interview.utils;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final String TAG = Utils.class.getSimpleName();
    AlertDialog mAlert;

    private static Utils mUtil = new Utils();

    private Utils() {
    }

    public static Utils getInstance() {
        return mUtil;
    }

    public boolean isValidEmail(String email) {
        Log.d(TAG, "isValidEmail email : " + email);
        if (TextUtils.isEmpty(email))
            return false;
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
        Pattern p = Pattern.compile(emailRegex);
        Matcher m = p.matcher(email);
        Log.d(TAG, "isValidEmail : " + m.matches());
        return m.matches();
    }

    public void showAlertDialog(Context context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mAlert != null && mAlert.isShowing()) {
                            mAlert.dismiss();
                        }
                    }
                });
        mAlert = builder.create();
        mAlert.show();
    }
}
